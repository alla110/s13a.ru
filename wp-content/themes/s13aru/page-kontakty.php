<?
/**
 * Template name: kontakty
 */
?>
<? get_header() ?>
<? $template_path = get_template_directory_uri(); ?>
    <div class="row">
        <div class="col-md-9">
            <div class="slider">
                <div class="slider_img">
                    <img src="<?=$template_path?>/img/slider0.jpg">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="menu1">

                <table border="0" id="menu1-table">
                     <tbody>
                    <tr id="social-plugin">
                        <td colspan="2">
                            <div class="pluso_img">
                                <img src="<?=$template_path?>/img/ric3 png. kont.jpg">
                                <img src="<?=$template_path?>/img/ric 3 png. ok.jpg">
                                <img src="<?=$template_path?>/img/ric 3 png. gugl.jpg">
                                <img src="<?=$template_path?>/img/ric3 png. feys.jpg">
                            </div>
                            <div class="pluso"
                                 data-background="transparent"
                                 data-options="medium,square,line,horizontal,nocounter,theme=04"
                                 data-services="vkontakte,odnoklassniki,facebook">
                            </div>
                            <div style="clear: both"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="menu1-icon">
                            <i class="fa fa-telegram fa-2x" aria-hidden="true"></i>
                        </td>
                        <td class="menu1-text"><a href="/kontakty">Контакты</a></td>
                    </tr>
                    <tr>
                        <td class="menu1-icon">
                            <i class="fa fa-bell fa-2x" aria-hidden="true"></i>
                        </td>
                        <td class="menu1-text"><a href="zvonki">Звонки</a></td>
                    </tr>
                    <tr>
                        <td class="menu1-icon">
                        <i class="fa fa-list fa-2x" aria-hidden="true"></i>
                        </td>
                        <td class="menu1-text"><a href="raspisanie">Расписание</a></td>
                    </tr>
                    <tr>
                        <td class="menu1-icon">
                            <i class="fa fa-cutlery fa-2x" aria-hidden="true"></i>
                        </td>
                        <td class="menu1-text"><a href="menu">Меню столовой</a></td>
                    </tr>
                    </tbody>
                </table>
                <hr id="hrhr">
                <div class="menu1-mini">
                    <table border="0" id="menu1-mini-table">
                        <tbody>
                        <tr>
                            <td class="menu1-mini-icon">
                                <i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i>
                            </td>
                            <td class="menu1-mini-text"><a href="obyavlenie">Объявления</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="main-container">
            <div class="col-md-9">
                <div class="content2">
                    <!-- fix strange margin-top of first block element -->
                    <div style="height: 0 !important; margin-top: 0 !important;
                    width: 100% !important; font-size: 0 !important;">strange</div>
                  <!--      <pre>
                            <?/* var_dump($post); */?>
                        </pre>
                    -->
                    <?
                            if (isset($post->ID)){
                                $post_id = $post->ID;
                            }
                            else {
                                $post_id = 4;
                            }
                        ?>
                    <? $cpost = get_post($post_id);  ?>

                    <h1><span><?=$cpost->post_title?></span>
                        <img src="<?=$template_path?>/img/lenta.gif"
                             style="width: 300px; float: right; margin-top: -40px;">
                    </h1>
                    <?=$cpost->post_content?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="menu2">
                    <table border="0" id="menu2-table">
                        <tbody>
                            <tr>
                                <td class="menu2-icon">
                                    <i class=" fa fa-user-circle-o fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="collective">Педколлектив</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class="fa fa-address-card fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="dlya-roditeley">Для родителей</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class=" fa fa-paste fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="plan">План работы</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class=" fa fa-newspaper-o fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="documents">Документы</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class=" fa fa-book fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="ustav">Устав</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class="fa fa-camera-retro fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="gallery">Галерея</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<script type='text/javascript' src='http://www.s13a.ru/wp-includes/js/wp-embed.min.js?ver=4.6.1'></script>
<? get_footer() ?>



