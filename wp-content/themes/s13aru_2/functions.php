<?php
/**
 * Created by PhpStorm.
 * User: alla110
 * Date: 13.11.2017
 * Time: 19:19
 */
/**
 * @param $template_path
 */
function side_bar_top($template_path)
{
    $bar = '
        <div class="col-md-3">
                <div class="menu1">
                <table border="0" id="menu1-table">
                    <tbody>
                    <tr id="social-plugin">
                        <td colspan="2">
                            <div class="pluso_img">
                            <a target="_blank" href="https://ok.ru/group/51639415996495">
                            <img src="' . $template_path . '/img/ok.jpg">
                             </a> 
                             <a target="_blank" href="https://vk.com">
                              <img src="' . $template_path .'/img/vk.jpg">
                              </a>
                                <img src="' . $template_path . '/img/ric 3 png. gugl.jpg">
                                <img src="' . $template_path . '/img/ric3 png. feys.jpg">
                            </div>
                            <div style="clear: both"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="menu1-icon">
                            <i class="fa fa-telegram fa-2x" aria-hidden="true"></i>
                        </td>
                        <td class="menu1-text"><a href="/kontakty">Контакты</a></td>
                    </tr>

                    <tr>
                        <td class="menu1-icon">
                            <i class="fa fa-bell fa-2x" aria-hidden="true"></i>
                        </td>
                        <td class="menu1-text"><a href="zvonki">Звонки</a></td>
                    </tr>
                    <tr>
                        <td class="menu1-icon">
                        <i class="fa fa-list fa-2x" aria-hidden="true"></i>
                        </td>
                        <td class="menu1-text"><a href="raspisanie">Расписание</a></td>
                    </tr>
                    <tr>
                        <td class="menu1-icon">
                            <i class="fa fa-cutlery fa-2x" aria-hidden="true"></i>
                        </td>
                        <td class="menu1-text"><a href="menu">Меню столовой</a></td>
                    </tr>
                    </tbody>
                </table>
                <hr id="hrhr">
                <div class="menu1-mini">
                    <table border="0" id="menu1-mini-table">
                        <tbody>
                        <tr>
                            <td class="menu1-mini-icon">
                                <i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i>
                            </td>
                            <td class="menu1-mini-text"><a href="obyavlenie">Объявления</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
    ';
    
    return $bar;
}

function slider_html($template_path)
{
    $slider = <<<html
        <div class="col-md-9">
            <div class="slider">
                <div class="slider_img">
                    <img src="$template_path/img/slider0.jpg">
                </div>
            </div>
        </div>

html;
    return $slider;
}

function menu2()
{
    return <<<html
            <div class="col-md-3">
                <div class="menu2">
                    <table border="0" id="menu2-table">
                        <tbody>
                            <tr>
                                <td class="menu2-icon">
                                    <i class=" fa fa-user-circle-o fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="collective">Педколлектив</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class="fa fa-address-card fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="dlya-roditeley">Для родителей</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class=" fa fa-paste fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="plan">План работы</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class=" fa fa-newspaper-o fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="documents">Документы</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class=" fa fa-book fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="ustav">Устав</a></td>
                            </tr>
                            <tr>
                                <td class="menu2-icon">
                                    <i class="fa fa-camera-retro fa-2x" aria-hidden="true"></i>
                                </td>
                                <td class="menu2-text"><a href="gallery">Галерея</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
html;

}

function get_cpost()
{
    global $post;

    if (isset($post->ID)){
        $post_id = $post->ID;
    }
    else {
        $post_id = 74;
    }
    return  get_post($post_id);
}

function replace_br($text)
{
    return str_replace(PHP_EOL, "<br>", $text);
}