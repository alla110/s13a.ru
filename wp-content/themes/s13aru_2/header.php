<!DOCTYPE html>
<? $template_path = get_template_directory_uri(); ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>
        <? bloginfo("name") ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?=$template_path?>/js/jquery-3.1.1.js"></script>
    <link rel="stylesheet" href="<?=$template_path?>/css/bootstrap.css">
    <script src="<?=$template_path?>/js/bootstrap.js"> </script>
    <link rel="stylesheet" type="text/css" href="<?=$template_path?>/css/style.css">
    <script src="https://use.fontawesome.com/2f30c805d9.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"
            type="text/javascript"></script>
    <script type="text/javascript">
    </script>
    <script type="text/javascript">
        ymaps.ready(init);
        var map13,
         myPlacemark;
        function init() {
            var my_coords = [48.487280, 38.816419];
            map13 = new ymaps.Map("map",{
                center: my_coords,
                zoom:15
            });
            myPlacemark = new ymaps.Placemark(my_coords, {
                hitContent: 'ул.Чайковского 6', balloonContent: 'Школа 13'
            });
            map13.geoObjects.add(myPlacemark);
        }
    </script>

    <script src="<?=$template_path?>/js/common.js"></script>
        <script>
            /**
             * fix lenta height if admin bar is visible
             */
            $(document).ready();

        </script>
    <? //wp_head(); ?>
</head>
<body>
<div id="lenta-top"></div>
<div class="container">
    <div class="row">
        <div class="logo-container">
            <div class="col-md-2">
                <div class="logo-img">
                    <img src="<?=get_template_directory_uri()?>/img/15222303.gif">
                </div>
            </div>
            <div class="col-md-8">
                <div class="logo-text">
                    <img src="<?=get_template_directory_uri()?>/img/descr.png">
                </div>
            </div>
            <div class="col-md-2 phone">
                <p id="phone">
                    <span style="opacity: 0.5;">(06442)</span>
                    <br>
                    <span style="color: #0136B7">2-52-52</span>
                </p>
            </div>
        </div>
    </div>
